ThisBuild / organization := "Viadrina"

name := "StudentEx"

ThisBuild / version := "0.1.0"

ThisBuild / scalaVersion := "2.13.6"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "2.0.0"
//libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "latest.integration"

scalacOptions += "-deprecation"

//(Compile,run) / mainclass := Some("StudentEx")

assembly / assemblyJarName := "StudentEx-0.1.0.jar"
