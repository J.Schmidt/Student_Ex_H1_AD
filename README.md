### StudentEx

Script zum Verschieben exmatrikulierter Studenten in OU mit weniger Rechten.

Das Script ist in Scala geschrieben und dient dem entgegengesetzten Zweck des Account Programmes:\
Es werden die Studenten ermittelt, die in HisInOne exmatrikuliert sind und diese werden in entsprechende OU‘s (nach- 2m, nach- 6m, nach-12m) in Active Directory verschoben. Vom Script werden keine Studentendaten gelöscht!

Nach Ablauf von 2 Monaten, d.h. beim Eintrag in die entsprechende OU, wird eine Email zur Information verschickt, sofern vorhanden auch an weitere hinterlegte Email-Adressen, nicht nur die universitäts-interne. Eine vorhandene Datei ordnung.txt wird in die Email eingebaut.

Die Voraussetzungen entsprechen denen des Account Programmes. Da keine Powershell Funktionen erforderlich sind, kann das Programm auf einem beliebigen Rechner ausgeführt werden.

Auf dem Entwicklungsrechner wird der normale Weg des Programmstarts verwendet:

* **sbt compile**
* **sbt run**

Zur Weitergabe des Scripts als alles umfassende jar-Datei wird diese wie folgt erzeugt:

* **sbt assembly**

Als VM kommt die aktuelle (2021) **LTS Version java-11** zum Einsatz.

Die Konfigurationsparameter
- AD_host, AD_admin, AD_password

sind für den Zugang zum Active Directory zuständig.

Die Konfigurationsparameter
- HisInOne_host, Soap_user, Soap_pw

sind für den Webservice Zugriff auf HisInOne zuständig.

Diese Parameter müssen in den Dateien config.scala bzw. setting.conf den Gegebenheiten angepasst werden. Beispiele sind in ~.example hinterlegt.

Mit dem Parameter -nomail kann die Email Ausgabe für Testzwecke deaktiviert werden, z.B. wenn die Funktionalität gegen einen Testserver ausprobiert wird. An der Viadrina werden die Emails über einen postfix Proxy verteilt.

#### Funktionsprinzip
Entsrechend der vorgegebenen Intervalle werden die exmatrikulierten Studenten aus HisInOne ausgelesen und dann in die passenden OU's verschoben. Nach 6 Monaten werden die Exchange-Parameter AcceptMessagesOnlyFrom (authOrig) und HiddenFromAdressList (msExchHideFromAddressLists) gesetzt.
