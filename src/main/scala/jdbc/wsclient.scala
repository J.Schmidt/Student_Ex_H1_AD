package main.scala.jdbc

import scala.xml.{Elem,Node,XML,PrettyPrinter,SAXParseException}
//import scala.xml._
import scala.io.Source
import scala.Exception

object WsClient {

  private def error(msg: String) = {
    println("SoapClient error: " + msg)
  }
    
  val Seq(host,username,password) = Seq("HisInOne_host","Soap_user","Soap_pw").map(Config.getProperty(_))
//    println(s"H1-Host: $host")
  val xmlHead = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
  val soapHeader = 
      <soapenv:Header>
        <wsse:Security soapenv:mustUnderstand="1"
            xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
          <wsse:UsernameToken>
            <wsse:Username>{username}</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">
              {password}
            </wsse:Password>
          </wsse:UsernameToken>
        </wsse:Security>
      </soapenv:Header>

  private def wrap(service: Service.Value, xml: Node): String = {
//    	val pp: PrettyPrinter = new PrettyPrinter(120,2);
     val wrapper =
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
          {soapHeader}
          <soapenv:Body xmlns:per={"http://www.his.de/ws/" + service}>
            {xml}
          </soapenv:Body>
        </soapenv:Envelope>
    xmlHead + wrapper
      
//    	xmlHead + pp.format(XML.loadString(wrapper.toString))
  }

//    def sendMessage(service: Service.Value, req: Elem) : Option[Elem] = 
//    	sendMessage(service, req, Func.empty)
    
  def sendMessage(service: Service.Value, req: Node, func: Func.Value) : Option[Elem] = {
    val url =  new java.net.URL(host + service)
    val conn = url.openConnection.asInstanceOf[java.net.HttpURLConnection]
    val outs = wrap(service, req).getBytes("UTF-8")
        
//        println("open Connection")
//        println("host: " + host)
//        val t0 = System.nanoTime()
//        val conn = if (host.startsWith("https://")) url.openConnection.asInstanceOf[javax.net.ssl.HttpsURLConnection]
//        		else url.openConnection.asInstanceOf[java.net.HttpURLConnection]
//        val t1 = System.nanoTime()
//        println("Elapsed time: " + (t1 - t0) + " ns")        
//        val conn = url.openConnection.asInstanceOf[javax.net.ssl.HttpsURLConnection]
//        val conn = url.openConnection.asInstanceOf[java.net.HttpURLConnection]
//        println("try now")
    try {
//import javax.net.ssl.HostnameVerifier
//            conn.setHostnameVerifier(new HostnameVerifier());
      conn.setRequestMethod("POST")
      conn.setDoOutput(true)
      conn.setRequestProperty("Content-Type", "text/xml")
      conn.setRequestProperty("Content-Length", outs.length.toString)
//            conn.setRequestProperty("Accept", "text/html")
        /*if (func != Func.empty)*/
      conn.setRequestProperty("SOAPAction",func.toString);
      conn.getOutputStream.write(outs)
//        val input = XML.load(conn.getInputStream)
      conn.getOutputStream.close
      Some (XML.load(conn.getInputStream))
    }
    catch {
      case ex: SAXParseException => error("parse error: " + ex); None
      case e: Exception => {
        error("post: " + e)
        val pp = new PrettyPrinter(80,4)
        val errStr = Source.fromInputStream(conn.getErrorStream).mkString
        error("post2:\n" + errStr)
//        		println(XML.load(err));
        println(pp.format(XML.loadString(errStr)))
            
//        		error("post2:\n" + pp.format(XML.load(err)))
        None
      }
    } 
//        finally {
//          error("post2:" + scala.io.Source.fromInputStream(conn.getErrorStream).mkString)))))
//        }
  }
}
