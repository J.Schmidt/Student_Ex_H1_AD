Voraussetzungen für den Betrieb
-------------------------------

### Active Directory

Erforderlich ist ein Funktionsaccount, das die Berechtigungen hat, Accounts zu generieren und Passwörter zu setzen. Geeignet ist z.B. der für HisInOne bereits existierende Account für das Login über den AD.

### HisInOne

Erforderlich ist ein SOAP User (Name, Passwort) mit folgenden Rechten:

-   **Kontaktdaten eines Studenten sehen**
-   **Studentendaten ansehen**
-   **Personen suchen**
-   **Accountdaten betrachten**
-   **(veraltet) Kontaktdaten sehen**
-   **Funktionen von Personen sehen**
-   **Personendaten ansehen**
-   **Rollen und Rechte anzeigen**
-   **Suche nach Rollen**
-   **Rollen anzeigen**
-   **Webservices nutzen**

Das sind die Mindestrechte für einen readonly Zugriff auf HisInOne, mit
denen das Script funktioniert. Das Passwort wird innerhalb HisInOne
gesetzt. Eine Nutzung dieses speziellen Users über den AD ist bisher
nicht möglich.

Die folgenden Webservices müssen erzeugt und aktiviert werden:

-   **AccountService**
-   **AddressService**
-   **PersonService**
-   **RightsAndRolesService**
-   **StudentService**

Nach bisheriger Erfahrung ist das nach jedem Update von HisInOne erneut
erforderlich!

### Java

An der Viadrina ist das Script mit der LTS Version Java 8 im Einsatz.
Die Änderungen für die aktuelle LTS Version Java 11 sind eingearbeitet.
Die Java Versionen auf dem Entwicklungsrechner und dem Script Rechner
müssen übereinstimmen.

